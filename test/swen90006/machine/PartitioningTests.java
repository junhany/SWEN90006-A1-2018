package swen90006.machine;

/*
* University of Melbourne
* SWEN90006 Software Testing and Reliability
* Semester 2 - 2018
* Assignment 1
* Author: Junhan Yang
* Login: junhany
* StudentID: 777738
*/

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class PartitioningTests
{
  //  EC refers to equivalent class.
  //  These test cases are created to test equivalent classes derived from
  //the test template tree drawn by the author from Task 1
  //using Equivalent Partitioning method.

  //  Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //  Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  //  For EC1:
  //  This test aims to test the Machine's behaviour
  //when there do not exist any RET instruction.
  //  The Machine's expected behaviour is to raise a NoReturnValueException.
  @Test(expected = NoReturnValueException.class)
    public void noRETInstructionTest()
    throws Throwable
  {
    List<String> lines = new ArrayList<String>();
    lines.add("MOV R1 10");
    Machine m = new Machine();
    m.execute(lines);
  }

  //  For EC2:
  //  This test aims to test the Machine's behaviour
  //when there exist multiple RET instructions.
  //  We need to use MOV to closely verify such condition, so we assume that
  //MOV executes correctly.
  //  The Machine's expected behaviour is to return the register's value from
  //the first RET, and the remaining instructions after the first RET
  //shouldn't be executed.
  @Test public void multiRETInstructionTest()
  {
    String err_msg = "Machine does not return the first RET value.";
    List<String> lines = new ArrayList<String>();
    lines.add("MOV R1 10");
    lines.add("RET R1");
    lines.add("MOV R2 20");
    lines.add("RET R2");
    Machine m = new Machine();
    assertEquals(err_msg, m.execute(lines), 10);
  }

  //  For EC3:
  //  This test aims to test the Machine's behaviour
  //when there exist unknown instructions that is
  //attempting to be executed.
  //  The Machine's expected behaviour is to raise an InvalidInstructionException.
  @Test(expected = InvalidInstructionException.class)
    public void executeUnknownInstructionTest()
    throws Throwable
  {
    String unknown_ins = "FOO R0 100";
    List<String> lines = new ArrayList<String>();
    lines.add(unknown_ins);
    lines.add("RET R0");
    Machine m = new Machine();
    m.execute(lines);
  }

  //  For EC4:
  //  This test aims to test the Machine's behaviour
  //when there exist unknown instructions that is not
  //attempting to be executed.
  //  The Machine's expected behaviour is return a value normally.
  @Test public void notExecuteUnknownInstructionTest()
  {
    String unknown_ins = "FOO R0 100";
    String err_msg = "Machine recognises unknown instruction " +
              "that is not even attempting to execute.";
    List<String> lines = new ArrayList<String>();
    lines.add("RET R0");
    lines.add(unknown_ins);
    Machine m = new Machine();
    assertEquals(err_msg, m.execute(lines), 0);
  }

  //  For EC5:
  //  This test aims to test the Machine's behaviour
  //when there exist register Rx such that x is int and x < 0.
  //  The Machine's expected behaviour is to raise an InvalidInstructionException.
  @Test(expected = InvalidInstructionException.class)
    public void smallResgisterTest()
    throws Throwable
  {
    List<String> lines = new ArrayList<String>();
    lines.add("RET R-1");
    Machine m = new Machine();
    m.execute(lines);
  }


  //  For EC6:
  //  This test aims to test the Machine's behaviour
  //when there exist register Rx such that x is int and x > 31.
  //  The Machine's expected behaviour is to raise an InvalidInstructionException.
  @Test(expected = InvalidInstructionException.class)
    public void largeResgisterTest()
    throws Throwable
  {
    List<String> lines = new ArrayList<String>();;
    lines.add("RET R32");
    Machine m = new Machine();
    m.execute(lines);
  }

  //  For EC7:
  //  This test aims to test the Machine's behaviour
  //when there exist some value val such that val is int and val > 65535 and
  //the instructions consists of MOV and RET instructions only.
  //  RET is assumed to be correct in this test case, so that MOV's execution can
  //be tested.
  //  The Machine's expected behaviour is to raise an InvalidInstructionException.
  @Test(expected = InvalidInstructionException.class)
    public void largeValMOVAndRETTest()
    throws Throwable
  {
    List<String> lines = new ArrayList<String>();
    lines.add("MOV R1 65536");
    lines.add("RET R1");
    Machine m = new Machine();
    m.execute(lines);
  }

  //  For EC8:
  //  This test aims to test the Machine's behaviour
  //when there exist some value val such that val is int and val < -65535 and
  //the instructions consists of MOV and RET instructions only.
  //  RET is assumed to be correct in this test case, so that MOV's execution can
  //be tested.
  //  The Machine's expected behaviour is to raise an InvalidInstructionException.
  @Test(expected = InvalidInstructionException.class)
    public void smallValMOVAndRETTest()
    throws Throwable
  {
    List<String> lines = new ArrayList<String>();
    lines.add("MOV R1 -65536");
    lines.add("RET R1");
    Machine m = new Machine();
    m.execute(lines);
  }

  //  For EC9:
  //  This test aims to test the Machine's behaviour
  //when there exist some value val such that val is int and 65535 <= val <= 65535
  //and the instructions consists of MOV and RET instructions only.
  //  RET is assumed to be correct in this test case, so that MOV's execution can
  //be tested.
  //  The Machine's expected behaviour is to raise an InvalidInstructionException.
  @Test public void normalValMOVAndRETTest()
  {
    String err_msg = "Machine does not execute MOV correctly.";
    List<String> lines = new ArrayList<String>();
    lines.add("MOV R1 0");
    lines.add("RET R1");
    Machine m = new Machine();
    assertEquals(err_msg, m.execute(lines), 0);
  }

  //  For EC10:
  //  This test aims to test the Machine's behaviour
  //for the correctness of executing combination of ADD, MOV and RET only.
  //  This test assumes the correctness of RET and MOV executions so that we
  //can test on ADD instruction.
  //  Passing this test means that ADD execute correctly.
  //  The Machine's expected behaviour is to return a value normally.
  @Test public void ADDTest()
  {
    String err_msg = "Machine does not execute ADD correctly.";
    List<String> lines = new ArrayList<String>();
    lines.add("MOV R0 2");
    lines.add("MOV R1 1");
    lines.add("ADD R3 R0 R1");
    lines.add("RET R3");
    Machine m = new Machine();
    assertEquals(err_msg, m.execute(lines), 3);
  }

  //  For EC11:
  //  This test aims to test the Machine's behaviour
  //for the correctness of executing combination of SUB, MOV and RET only.
  //  This test assumes the correctness of RET and MOV executions so that we
  //can test on SUB instruction.
  //  Passing this test means that SUB execute correctly.
  //  The Machine's expected behaviour is to return a value normally.
  @Test public void SUBTest()
  {
    String err_msg = "Machine does not execute SUB correctly.";
    List<String> lines = new ArrayList<String>();
    lines.add("MOV R0 3");
    lines.add("MOV R1 1");
    lines.add("SUB R3 R0 R1");
    lines.add("RET R3");
    Machine m = new Machine();
    assertEquals(err_msg, m.execute(lines), 2);
  }


    //  For EC12:
    //  This test aims to test the Machine's behaviour
    //for the correctness of executing combination of MUL, MOV and RET only.
    //  This test assumes the correctness of RET and MOV executions so that we
    //can test on MUL instruction.
    //  Passing this test means that MUL execute correctly.
    //  The Machine's expected behaviour is to return a value normally.
    @Test public void MULTest()
    {
      String err_msg = "Machine does not execute MUL correctly.";
      List<String> lines = new ArrayList<String>();
      lines.add("MOV R0 3");
      lines.add("MOV R1 -1");
      lines.add("MUL R3 R0 R1");
      lines.add("RET R3");
      Machine m = new Machine();
      assertEquals(err_msg, m.execute(lines), -3);
    }

  //  For EC13:
  //  This test aims to test the Machine's behaviour
  //for the correctness of executing combination of JMP 1, MOV and RET only.
  //  This test assumes the correctness of RET and MOV executions so that we
  //can test on JMP instruction.
  //  Passing this test means that JMP 1 is a no-op.
  //  The Machine's expected behaviour is to return a value normally.
  @Test public void JMPONETest()
  {
    String err_msg = "Machine does not execute JMP 1 correctly.";
    List<String> lines = new ArrayList<String>();
    lines.add("JMP 1");
    lines.add("MOV R0 10");
    lines.add("RET R0");
    Machine m = new Machine();
    assertEquals(err_msg, m.execute(lines), 10);
  }

  //  For EC14:
  //  This test aims to test the Machine's behaviour
  //for the correctness of executing combination of JMP, MOV and RET only.
  //  This test assumes the correctness of RET and MOV executions so that we
  //can test on JMP instruction.
  //  Passing this test means that JMP -2 will cause pc < 0 which breaks the
  //loop and ends the execution immdiately without returning a value by RET.
  //  The Machine's expected behaviour is to raise a NoReturnValueException.
  @Test(expected  = NoReturnValueException.class)
    public void JMPNegValAndSmallNewpcTest()
    throws Throwable
  {
    List<String> lines = new ArrayList<String>();
    lines.add("MOV R0 10");
    lines.add("JMP -2");
    lines.add("RET R0");
    Machine m = new Machine();
    m.execute(lines);
  }

  //  For EC15:
  //  This test aims to test the Machine's behaviour
  //for the correctness of executing combination of JMP, MOV and RET only.
  //  This test assumes the correctness of RET and MOV executions so that we
  //can test on JMP instruction.
  //  Passing this test means that JMP 2 skip the next instruction and rightaway
  //returning a value by RET.
  //  The Machine's expected behaviour is to return a value normally.
  @Test public void JMPPosiValAndNormalNewpcTest()
  {
    String err_msg = "Machine does not execute JMP correctly with val > 1.";
    List<String> lines = new ArrayList<String>();
    lines.add("JMP 2");
    lines.add("MOV R0 10");
    lines.add("RET R0");
    Machine m = new Machine();
    assertEquals(err_msg, m.execute(lines), 0);
  }


    //  For EC16:
    //  This test aims to test the Machine's behaviour
    //for the correctness of executing combination of JMP, MOV and RET only.
    //  This test assumes the correctness of RET and MOV executions so that we
    //can test on JMP instruction.
    //  Passing this test means that JMP 3 will cause pc > progLength which breaks
    //the loop and ends the execution immdiately without returning a value by RET.
    //  The Machine's expected behaviour is to raise a NoReturnValueException.
    @Test(expected  = NoReturnValueException.class)
      public void JMPPosValAndLargeNewpcTest()
      throws Throwable
    {
      List<String> lines = new ArrayList<String>();
      lines.add("JMP 3");
      lines.add("MOV R0 10");
      lines.add("RET R0");
      Machine m = new Machine();
      m.execute(lines);
    }

  //  For EC17:
  //  This test aims to test the Machine's behaviour
  //for the correctness of executing combination of JZ, MOV and RET only.
  //  This test assumes the correctness of RET and MOV executions so that we
  //can test on JZ instruction.
  //  Passing this test means that JZ with non-zero register is a no-op,
  //and the pc will increment by 1.
  //  The Machine's expected behaviour is to return a value normally.
  @Test public void JZNonZeroTest()
  {
    String err_msg = "Machine does not execute JZ correctly with non-zero register value.";
    List<String> lines = new ArrayList<String>();
    lines.add("MOV R0 5");
    lines.add("JZ R0 10");
    lines.add("RET R0");
    Machine m = new Machine();
    assertEquals(err_msg, m.execute(lines), 5);
  }

  //  For EC18:
  //  This test aims to test the Machine's behaviour
  //for the correctness of executing combination of JZ, MOV and RET only.
  //  This test assumes the correctness of RET and MOV executions so that we
  //can test on JZ instruction.
  //  Passing this test means that JZ with zero register value will add val to pc,
  //only if 0 <= pc < progLength.
  //  The Machine's expected behaviour is to return a value normally.
  @Test public void JZZeroAndNormalNewpcTest()
  {
    String err_msg = "Machine does not execute JZ correctly with zero register value.";
    List<String> lines = new ArrayList<String>();
    lines.add("JZ R0 2");
    lines.add("MOV R0 5");
    lines.add("RET R0");
    Machine m = new Machine();
    assertEquals(err_msg, m.execute(lines), 0);
  }

  //  For EC19:
  //  This test aims to test the Machine's behaviour
  //for the correctness of executing combination of JZ, MOV and RET only.
  //  This test assumes the correctness of RET and MOV executions so that we
  //can test on JZ instruction.
  //  Passing this test means that JZ with zero register will add val to pc,
  //but causing pc < 0 so breaking the loop without executing RET.
  //  The Machine's expected behaviour is to raise a NoReturnValueException.
  @Test(expected  = NoReturnValueException.class)
    public void JZZeroAndSmallNewpcTest()
    throws Throwable
  {
    List<String> lines = new ArrayList<String>();
    lines.add("JZ R0 -1");
    lines.add("MOV R0 5");
    lines.add("RET R0");
    Machine m = new Machine();
    m.execute(lines);
  }

  //  For EC20:
  //  This test aims to test the Machine's behaviour
  //for the correctness of executing combination of JZ, MOV and RET only.
  //  This test assumes the correctness of RET and MOV executions so that we
  //can test on JZ instruction.
  //  Passing this test means that JZ with zero register will add val to pc,
  //but causing pc >= progLength so breaking the loop without executing RET.
  //  The Machine's expected behaviour is to raise a NoReturnValueException.
  @Test(expected  = NoReturnValueException.class)
    public void JZZeroAndLargeNewpcTest()
    throws Throwable
  {
    List<String> lines = new ArrayList<String>();
    lines.add("JZ R0 3");
    lines.add("MOV R0 5");
    lines.add("RET R0");
    Machine m = new Machine();
    m.execute(lines);
}

  //  For EC21:
  //  This test aims to test the Machine's behaviour
  //for the correctness of executing combination of DIV, MOV and RET only.
  //  This test assumes the correctness of RET and MOV executions so that we
  //can test on DIV instruction.
  //  Passing this test means that DIV by zero is a no-op.
  //  The Machine's expected behaviour is to return a value normally.
  @Test public void DIVByZeroTest()
  {
    String err_msg = "Machine does not execute DIV by Zero correctly.";
    List<String> lines = new ArrayList<String>();
    lines.add("MOV R0 9");
    lines.add("MOV R1 0");
    lines.add("DIV R3 R0 R1");
    lines.add("RET R3");
    Machine m = new Machine();
    assertEquals(err_msg, m.execute(lines), 0);
  }

  //  For EC22:
  //  This test aims to test the Machine's behaviour
  //for the correctness of executing combination of DIV, MOV and RET only.
  //  This test assumes the correctness of RET and MOV executions so that we
  //can test on DIV instruction.
  //  Passing this test means that DIV by non-zero will obtain result normally.
  //  The Machine's expected behaviour is to return a value normally.
  @Test public void DIVByNonZeroTest()
  {
    String err_msg = "Machine does not execute DIV by non-zero correctly.";
    List<String> lines = new ArrayList<String>();
    lines.add("MOV R0 9");
    lines.add("MOV R1 3");
    lines.add("DIV R3 R0 R1");
    lines.add("RET R3");
    Machine m = new Machine();
    assertEquals(err_msg, m.execute(lines), 3);
  }

  //  For EC23:
  //  This test aims to test the Machine's behaviour
  //for the correctness of executing combination of STR, LDR, MOV and RET only.
  //  This test assumes the correctness of RET and MOV executions so that we
  //can test on STR and LDR instructions.
  //  Passing this test means that STR and LDR with b + v > 65535 is a no-op.
  //  The Machine's expected behaviour is to return a value normally.
  @Test public void STRAndLDRLargeRegPlusValTest()
  {
    String err_msg = "Machine does not execute STR and/or LDR correctly with b + v > 65535.";
    List<String> lines = new ArrayList<String>();
    lines.add("MOV R0 65535");
    lines.add("MOV R1 5");
    lines.add("STR R0 1 R1");
    lines.add("LDR R2 R0 1");
    lines.add("RET R2");
    Machine m = new Machine();
    assertEquals(err_msg, m.execute(lines), 0);
  }

  //  For EC24:
  //  This test aims to test the Machine's behaviour
  //for the correctness of executing combination of STR, LDR, MOV and RET only.
  //  This test assumes the correctness of RET and MOV executions so that we
  //can test on STR and LDR instruction.
  //  Passing this test means that STR and LDR with b + v < 0 is a no-op.
  //  The Machine's expected behaviour is to return a value normally.
  @Test public void STRSmallRegisterValuePlusValTest()
  {
    String err_msg = "Machine does not execute STR and/or LDR correctly with b + v < 0.";
    List<String> lines = new ArrayList<String>();
    lines.add("MOV R0 4");
    lines.add("MOV R1 5");
    lines.add("STR R0 -5 R1");
    lines.add("LDR R2 R0 -5");
    lines.add("RET R2");
    Machine m = new Machine();
    assertEquals(err_msg, m.execute(lines), 0);
  }

  //  For EC25:
  //  This test aims to test the Machine's behaviour
  //for the correctness of executing combination of STR, LDR, MOV and RET only.
  //  This test assumes the correctness of RET and MOV executions so that we
  //can test on STR and LDR instruction.
  //  Passing this test means that STR and LDR with 0 <= b + v <= 65535 works normally
  //by assigning value on memory[b + v].
  //  The Machine's expected behaviour is to return a value normally.
  @Test public void STRNormalRegisterValuePlusValTest()
  {
    String err_msg = "Machine does not execute STR and/or LDR correctly 0 <= b + v <= 65535.";
    List<String> lines = new ArrayList<String>();
    lines.add("MOV R0 -1");
    lines.add("MOV R1 2");
    lines.add("STR R0 1 R1");
    lines.add("LDR R2 R0 1");
    lines.add("RET R2");
    Machine m = new Machine();
    assertEquals(err_msg, m.execute(lines), 2);
  }
}

/*------------------------- END of Partitioning Tests -------------------------*/
